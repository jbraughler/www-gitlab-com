---
layout: handbook-page-toc
title: "TAM Quarterly Business Review"
description: "Instructions for TAM Managers on presenting their QBR decks."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

The TAM Manager is responsible for developing and delivering a [Quarterly Business Review](https://about.gitlab.com/handbook/sales/qbrs/) for their team each quarter.

The Quarterly Business Review is a tool to evaluate, report on, learn from the actions and performance of the team over the quarter, and share asks and priorities for the next quarter. TAM QBRs [follow the same schedule as Sales QBRs](https://about.gitlab.com/handbook/sales/qbrs/#qbr-schedules), and should be [presented during Sales QBR sessions](#presenting-a-tam-qbr) for the TAM Manager's region.

## Preparing a TAM QBR

Each quarter, a TAM QBR presentation template is developed to assist managers in developing their QBR with emphasis on the details that Customer Success and the TAM organization is focused on. Use this template as a starting point, and add additional details that are relevant to your team's story.

## Presenting a TAM QBR

The schedule of TAM QBR presentations may vary each quarter, but typically QBRs are presented by each manager to the rest of the TAM leadership in a dedicated session, and then each TAM Manager presents their QBR during their region's Sales QBR sessions.

When presenting your QBR during Sales QBR sessions, you may have a very limited amount of time for your presentation. Have a plan to present your QBR with a focus on the highlights, and consider assembling an abbreviated version of your presentation.
