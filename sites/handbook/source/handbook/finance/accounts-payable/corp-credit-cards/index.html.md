---
layout: handbook-page-toc
title: Corporate Credit Cards
decription: >-
  This page contains GitLab's Corporate Credit Card guidelines and procedures.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page contains GitLab's Corporate Credit Card guidelines and procedures. If there are any questions, please reach out to **#corp-creditcard-holders** Slack channel or email **ap@gitlab.com**.

## <i id="biz-tech-icons" class="far fa-paper-plane"></i> Getting Started
1. To request a new Corporate Credit Card, please create a Finance issue (tagging `@accounts-payable`) for approval.
1. Once the Finance issue is approved, your card will be ordered and mailed to you. You will need to follow the instructions sent with the card to activate it.
1. Once activated, please reach to **#corp-creditcard-holders** to let AP know your card is activated.  AP will then set the card up in Expensify.
1. As soon as you start charging the card, the charges will appear in Expensify as a new report under your name. You will be able to code and attach receipts to each purchase.

## <i id="biz-tech-icons" class="fas fa-stream"></i> Guidelines for Card Use
- Corporate Credit Cards are mainly to be used for Travel and Entertainment purposes only.
- Corporate Credit Cards should not be used to bypass the Procurement process.
- Purchases on your card should not exceed $2,500 for each transaction.  If your purchase will be over $2,500, you will need to enter a Purchase Requisition in Coupa. AP prefers to pay vendors by ACH whenever possible.  For reference to get started with Coupa for a vendor who can be paid by normal invoicing and payment methods, see [Coupa End Users Guide](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/coupa-guide/).
- However, if your purchase will be over $2,500 and needs to be paid by credit card, please follow the instructions to enter a Purchase Requisition into Coupa to be paid by [Coupa Virtual Card](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/coupa-virtual-cards/).
- All credit card reports must be submitted to their manager.  Then the manager is to review/approve and then submit to Misty Brown (on behalf of the AP team to process the report).
- Any reports not submitted for one month will be subject to management review and cards could be taken away for non-compliance.
- Any reports with continuous violations to our policy will be subject to management review and could be taken away for non-compliance.

## <i id="biz-tech-icons" class="fas fa-bullseye"></i> Items not permitted to be purchased on Corporate Credit Cards
- Software subscriptions. Please follow the process regarding [Individual Use Software](https://about.gitlab.com/handbook/finance/procurement/personal-use-software/).
- Computer equipment and hardware. These purchases must go through the normal employee expense reimbursement process.
- All purchases relating to Training/Tuition/Conferences and Membership fees. These purchases must follow the [Growth and Development Benefit](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) process.
- Employee Gifts. These purchases must go through the normal employee expense reimbursement process.
- Personal expenses.
- Hotels & Airfares & Uber & Rental Cars: These are not allowed to be purchased under our CC program and need to run through the employee reimbursement process for proper management approvals and airfare should be purchased through TripActions.

#### Gift Cards

1. Gift Card Purchases to team members are not allowed, as they are taxable per IRS guidelines.  If gift card purchases are made, it requires pre-approval per Policy and the team member who receives the gift card will be taxed accordingly.
1. Gift Card Purchases to non-Gitlab employees will need to notify their manager and employers per IRS rules, as well as obtaining approval from the PAO of Finance or the VP Corporate Controller approval before purchasing.
1. Non-cash team member gifts of certain value (over $75 per year), such as a wine bottle, are taxable.

## <i id="biz-tech-icons" class="fas fa-stream"></i> Expensify Reports and Coding
- Make sure you choose **Gitlab-Amex** policy when submitting your Corporate Credit Card report in Expensify.
- You must  **“Submit”** your report in order to close it out each month.  If a report remains **“Open”**, the Corporate Credit Card charges will keep being added to that report and will not generate a new report for the following month.
- Do not commingle any personal reimburseable expenses into your Corporate Credit Card reports.  These reports are tied directly to the Corporate Credit Card statement, so they must remain separate.
- Receipts should be attached for all purchases in Expensify. The receipts must be fully legible and indicate the description of goods or services, total purchase amount and taxes.
- If applicable, add **expense tags** under **"Classifications"** to the related purchase. Common expense tag examples include company contributes, marketing campaigns and professional service engagements.
- If the purchase should be charged to another entity besides Gitlab Inc, you must specify the entity in the expense report line item.

## <i id="biz-tech-icons" class="fas fa-bullseye"></i> Card Holder Responsibility
- Card Holders are responsible for providing all supporting documents.
- All transactions made through the corporate credit card shall be filed via Expensify by the Card holder with proper coding and should be submitted to their manager twice a month, by the 15th and by the last day of the month.  Managers are to approve and then forward to the AP Team for final approval by the 1st of the following month.
- Card Holders must follow Gitlab’s Code of Business Conduct and Ethics as the Corporate Credit Cards are considered GitLab property. Please refer to GitLab's [Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/).
- To cancel a card or to report a card that is lost/stolen, please reach out to **#corp-creditcard-holders** Slack channel or email **ap@gitlab.com**.
- Failure to follow the guidelines will result in your card being pulled.

## <i id="biz-tech-icons" class="far fa-question-circle"></i> Any Questions?  
Please reach out to **#corp-creditcard-holders** or **email ap@gitlab.com**.





