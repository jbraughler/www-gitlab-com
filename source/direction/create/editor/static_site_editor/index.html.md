---
layout: markdown_page
title: "Category Direction - Static Site Editor"
description: "Static websites are a flexible and performant option for many types of sites including blogs, documentation portals, and portfolios. The Static Site Editor aims to make editing content on these websites a delightful and intuitive experience."
canonical_path: "/direction/create/editor/static_site_editor/"
---

- TOC
{:toc}

## Static Site Editor

|  Stage   |   Maturity  |   Content Last Reviewed   |
|  ---   |   ---   |   ---   |
| [Create](/direction/dev/index.html#create) | [Viable](/direction/maturity/) | `2021-11-24` |

## Introduction and how you can help

This is the category direction page for the Static Site Editor in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by Sr. Product Manager, [Eric Schurter](https://about.gitlab.com/company/team/#ericschurter). More information about the Editor group's priorities and direction can be found on the [Editor group direction page](/direction/create/editor/).

Established as a category in late 2019, the Static Site Editor quickly reached Viable maturity and provides an alternative editing experience for non-developer personas. After researching and developing the Content Editor, originally architected as the next generation WYSIWYG Markdown editor for the Static Site Editor, we have made the decision to deprecate the Static Site Editor. While the category and feature are being removed, the value of making Markdown content more easily accessible continues to be one of the highest priorities of the Editor group. By focusing on delivering intuitive and extensible Markdown editing across GitLab and investing in the Pages feature, we plan to realize the vision originally put forth by the Static Site Editor, but do so in a more focused and integrated way. 

Sharing your questions or feedback directly on [GitLab.com](https://gitlab.com/) is the best way to contribute to our category direction. Please don't hesitate to leave a comment on our [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Astatic%20site%20editor) or [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AStatic%20Site%20Editor) and, of course, we welcome community contributions. If you'd rather provide feedback privately or schedule a video call, you can email our Product Manager, ([Eric Schurter](mailto:eschurter@gitlab.com)).

## Overview

Our mission is to ensure that creating and editing content on [static websites](https://en.wikipedia.org/wiki/Static_web_page) does not require deep knowledge of any particular templating language, Markdown syntax, or git branching workflows. The Static Site Editor category is focused on delivering a familiar and intuitive content editing experience that enables efficient collaboration on static sites across all users, on any platform, regardless of their technical experience.

Static websites are a flexible and performant option for many types of sites, including blogs, documentation portals, and portfolios. These use cases necessitate the ability for content to be edited quickly, frequently, and often away from a desktop computer. For this reason, the goal is to deliver a delightful content editing experience on desktops, tablets, and mobile devices.

### Target Audience

- **[Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer):** While collaboration on static website content should be accessible to personas with less technical expertise, developers like Sasha are the ones who have to create and configure the project in the first place. Even with her technical ability, after configuring the project, Sasha may find it easier to edit content using a more streamlined, content-focused editor instead of a code editor.
- **GitLab Team Members:** The GitLab handbook is a prime example of a static site and every member of the GitLab community should be able to actively contribute to it, whether or not they have the relevant technical knowledge. As this category reaches a lovable level of maturity, it will be possible to create and edit the content on the GitLab handbook without writing a single line of code.
- **UX Writers, Technical Writers, Copywriters:** Authors of website content should be focused on the content itself, not on remembering markdown syntax or specifics around templating markup. These contributors are at best slowed down and, at worst, entirely prevented from doing their job when the technology doesn't adapt to their workflow and technical ability.
- **Product Managers, Leadership, Stakeholders:** As companies move to a more distributed way of working, processes and documentation becomes increasingly important. Contributions to this type of content come from everyone. Stakeholders and members of leadership need a fast, reliable, and accessible way to collaborate with their team.

### Challenges to address

#### Creating, configuring, and hosting a static website

From choosing a programming language and a static site generator to configuring a deployment pipeline and managing their domain, users are required to make a series of decisions and piece together multiple services to create and deploy a static website from scratch. If they want to collaborate on content, yet another service might be added into the mix to manage content and provide an editing experience accessible by everyone. Users need a single place to create, configure, edit, and host their static web content.

This is a workflow that GitLab is well positioned to address and the Static Site Editor is one of the last remaining pieces needed to deliver this end-to-end experience. This opportunity is one that we have not fully addressed in the current iteration of the Static Site Editor, but it will be an area where we will be investing time to [validate the problem space](https://about.gitlab.com/handbook/product-development-flow/#validation-phase-2-problem-validation).

#### Editing content on an existing static page

Collaborating on static web content can be a challenging, even intimidating experience for a variety of reasons.

- Users may not be able to find the content they want to edit in a complex project structure
- Users may not be familiar with development environments or code editors and can be intimidated by the editing experience
- Users may not be familiar with Markdown and how it relates to the final HTML output which can lead to a lack of confidence in their edits being formatted correctly
- Users may not understand the git workflow and how to create branches, commit changes, and open a merge request for review
- Small, accidental errors in syntax can cause builds to fail, making users feel like the risk of introducing small, accidental syntax errors outweighs the benefits of them contributing directly

To get around these problems, developers have to decide whether they are either going to force their contributors to learn the technical skills required to edit content or integrate with a headless CMS or other abstraction of the data model. The former can be prohibitive to collaboration and the latter can be costly and complicated to maintain.

#### Creating new content on a static website

Workflows that require creating, moving, or deleting content on a static website suffer from the same challenges as editing, but introduce additional complexities, especially for users who aren't familiar with the project's information architecture and directory structure. Users can struggle to:

- Know which layout template to use for a new page
- Understand where to put new pages
- Understand where to put images and other assets
- Manage redirects and update links to existing content that has been moved or deleted
- Create content meant to be re-used across multiple pages on the website

Similar workarounds are in place for these workflows, but often rely on either learned behavior or managing content in only the most advanced, fully-featured content management systems.

#### Publishing quality content to a static website

Once the edits have been made, the content still has to get built and deployed before it is available on the web. This can be a time-consuming process and one that can fail in many places along the way.

- Users want to feel confident that their changes will render correctly before starting the deployment process
- Users want to be notified if anything goes wrong along the way
- Users want to be able to submit proposed changes for review before publishing to the website

The Git-based workflow is perfectly suited to handle these challenges, but it does require a significant amount of prior knowledge around version control and local development environments. A proper CMS can abstract all of this away from the user, but often does so at the expense of flexibility, preventing those who do know the inner-workings of Git from performing advanced tasks.

## Where we are Headed

The Static Site Editor may be deprecated, but the vision lives on in the Content Editor and Pages direction. We will be looking to make WYSIWYG Markdown editing available wherever Markdown is written in GitLab and enhance the Pages experience to deliver a more comprehensive solution for creating, managing, authoring, and deploying static sites. 


### User success metrics

It doesn't matter as much to us whether a merge request gets deployed and while we hope everyone gets a chance to try out the Static Site Editor, simply opening up the editor doesn't mean you've contributed any content. Our primary [performance indicator](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#createstatic-site-editor---gmau---mau-that-committed-via-the-sse) revolves entirely around a commit action, which is an indication of a successful contribution.

We want to encourage more people to commit more often. This would mean that users of services outside of GitLab to edit content can be brought into the platform to streamline their collaboration. It also means that existing users will be exposed to more features GitLab has to offer across the many stages of the DevOps workflow.

### Why is this important?

A delightful, accessible editing experience is the last piece of the puzzle for enabling end-to-end management of static sites within GitLab. The end goal is to streamline collaboration with engineers by bringing users into GitLab rather than spreading the work across multiple products, resulting in both cost and time efficiencies for our users.

## Competitive Landscape

Products that directionally inspire our vision for editing static sites:

- [Netlify CMS](https://www.netlifycms.org/)
- [Forestry.io](https://forestry.io/)
- [TinaCMS](https://tinacms.org/)
- [Stackbit](https://www.stackbit.com/)

Products that help people in organizations collaborate on information together like our Handbook:

- [Confluence](https://www.atlassian.com/software/confluence)
- [Notion](https://www.notion.so/)

<!-- ## Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!-- ### Top user issue(s) -->
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

<!-- ### Top internal customer issue(s) -->
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.-->

<!-- ### Top Strategy Item(s) -->
<!-- What's the most important thing to move your strategy forward?-->
